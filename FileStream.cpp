#include "FileStream.h"

FileStream::FileStream()
{
	char *pos;
	char location[50] = "";
	printf("file's location pls: ");
	fgets(location, 50, stdin);
	location[strlen(location) - 1] = 0;
	f = fopen(location, "w+");
}

FileStream::FileStream(char* s)
{
	f = fopen(s, "w+");
}

FileStream::~FileStream()
{
	fclose(f);
}

FileStream& FileStream::operator<<(const char *str)
{
	fprintf(f, str);
	return *this;
}


FileStream& FileStream::operator<<(int num)
{	
	char res[50] = { 0 };
	fprintf(f, "%d",  num);
	return *this;
}

FileStream& FileStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}

void FileStream::endline()
{
	fprintf(f, "\n");
}