#include "OutStream.h"
#pragma once
class FileStream : public OutStream
{
	FILE* f;
public:
	FileStream();
	~FileStream();
	FileStream(char* s);

	virtual FileStream& operator<<(const char *str);
	virtual FileStream& operator<<(int num);
	virtual FileStream& operator<<(void(*pf)());
	void endline();
};

